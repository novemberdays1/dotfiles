# dotfiles
Configuration dotfiles for my desktop like Neovim, tmux, i3wm, polybar, dunst, picom, and etc.

With your desktop's package manager, install yadm:
```
sudo pacman -S yadm
```

Then clone the repository with yadm
